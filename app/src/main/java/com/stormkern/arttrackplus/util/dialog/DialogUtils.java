package com.stormkern.arttrackplus.util.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.WindowManager;

import com.stormkern.arttrackplus.R;
import com.stormkern.arttrackplus.util.Common;

/**
 * Created by StormKern on 3/12/2015.
 */
public class DialogUtils {

    public static void showDialog(Dialog dialog) {
        try {
            if (dialog != null) dialog.show();
        } catch (WindowManager.BadTokenException bte) {
            //do nothing, user backed out before dialog was displayed
        } catch (Exception ex) {
            //Log.e(Common.TAG, "Could not display dialog", ex);
        }
    }

    public static void dismissDialog(DialogInterface dialog) {
        try {
            if (dialog != null) dialog.dismiss();
        } catch (WindowManager.BadTokenException bte) {
            //do nothing, user backed out before dialog was dismissed
        } catch (Exception ex) {
            //Log.e(Common.TAG, "Could not dismiss dialog", ex);
        }
    }

    public static AlertDialog createYesNoDialog(Context context, String title, String message, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        if (message != null) builder.setMessage(message);
        builder.setPositiveButton(R.string.yes, listener);
        builder.setNegativeButton(R.string.no, listener);
        builder.setCancelable(false);
        return builder.create();
    }

    public static AlertDialog createYesNoDialog(Context context, @StringRes int title, @StringRes int message, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.yes, listener);
        builder.setNegativeButton(R.string.no, listener);
        builder.setCancelable(false);
        return builder.create();
    }

    public static AlertDialog createYesNoDialogWithText(Context context, CharSequence title, CharSequence message, DialogInterface.OnClickListener listener, String yesText, String noText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        if (message != null) builder.setMessage(message);
        builder.setPositiveButton(yesText, listener);
        builder.setNegativeButton(noText, listener);
        builder.setCancelable(false);
        return builder.create();
    }

    public static AlertDialog createYesNoDialogWithText(Context context, @StringRes int title, @StringRes int message, DialogInterface.OnClickListener listener, @StringRes int yesText, @StringRes int noText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(yesText, listener);
        builder.setNegativeButton(noText, listener);
        builder.setCancelable(false);
        return builder.create();
    }

    public static void showErrorDialog(Context context, String title, String err) {
        try {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle(title);
            b.setMessage(err);
            b.setPositiveButton(R.string.ok, null);
            b.setCancelable(false);
            b.show();
        } catch (Exception ex) {
//            Common.logException("Unable to display error dialog", ex);
        }
    }

    public static void showErrorDialog(Context context, @StringRes int title, @StringRes int err) {
        try {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle(title);
            b.setMessage(err);
            b.setPositiveButton(R.string.ok, null);
            b.setCancelable(false);
            b.show();
        } catch (Exception ex) {
//            Common.logException("Unable to display error dialog", ex);
        }
    }

    public static void showErrorDialog(Context context, String title, String error, DialogInterface.OnClickListener listener) {
        try {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle(title);
            b.setMessage(error);
            b.setPositiveButton(R.string.ok, listener);
            b.setCancelable(false);
            b.show();
        } catch (Exception ex) {
//            Common.logException("Unable to display error dialog with listener", ex);
        }
    }

    public static void showErrorDialog(Context context, @StringRes int title, @StringRes int error, DialogInterface.OnClickListener listener) {
        try {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle(title);
            b.setMessage(error);
            b.setPositiveButton(R.string.ok, listener);
            b.setCancelable(false);
            b.show();
        } catch (Exception ex) {
//            Common.logException("Unable to display error dialog with listener", ex);
        }
    }

    public static void showInfoDialog(Context context, String title, String message) {
        try {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle(title);
            b.setMessage(message);
            b.setPositiveButton(R.string.ok, null);
            b.setCancelable(false);
            b.show();
        } catch (Exception ex) {
//            Common.logException("Unable to display info dialog", ex);
        }
    }

    public static void showInfoDialog(Context context, @StringRes int title, @StringRes int message) {
        try {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle(title);
            b.setMessage(message);
            b.setPositiveButton(R.string.ok, null);
            b.setCancelable(false);
            b.show();
        } catch (Exception ex) {
//            Common.logException("Unable to display info dialog", ex);
        }
    }

    public static void showInfoDialog(Context context, String title, String message, DialogInterface.OnClickListener listener) {
        try {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle(title);
            b.setMessage(message);
            b.setPositiveButton(R.string.ok, listener);
            b.setCancelable(false);
            b.show();
        } catch (Exception ex) {
//            Common.logException("Unable to display info dialog", ex);
        }
    }

    public static void showInfoDialog(Context context, @StringRes int title, @StringRes int message, DialogInterface.OnClickListener listener) {
        try {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle(title);
            b.setMessage(message);
            b.setPositiveButton(R.string.ok, listener);
            b.setCancelable(false);
            b.show();
        } catch (Exception ex) {
//            Common.logException("Unable to display info dialog", ex);
        }
    }

    public static AlertDialog showInfoDialogWithThreeButtons(Context context, String title, String message, String buttonPositiveText, String buttonNeutralText, String buttonNegativeText, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder b = new AlertDialog.Builder(context);
        b.setTitle(title);
        b.setMessage(message);
        b.setPositiveButton(buttonPositiveText, listener);
        b.setNeutralButton(buttonNeutralText, listener);
        b.setNegativeButton(buttonNegativeText, listener);
        b.setCancelable(false);
        return b.create();
    }

    public static AlertDialog showInfoDialogWithThreeButtons(Context context, @StringRes int title, @StringRes int message, @StringRes int buttonPositiveText, @StringRes int buttonNeutralText, String buttonNegativeText, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder b = new AlertDialog.Builder(context);
        b.setTitle(title);
        b.setMessage(message);
        b.setPositiveButton(buttonPositiveText, listener);
        b.setNeutralButton(buttonNeutralText, listener);
        b.setNegativeButton(buttonNegativeText, listener);
        b.setCancelable(false);
        return b.create();
    }
}
