package com.stormkern.arttrackplus.util.permission;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

/**
 * Created by StormKern on 11/1/2015.
 */
public class PermissionsHelper {
    public static boolean hasPermissionGranted(Context context, String permission) {
        int check = ContextCompat.checkSelfPermission(context, permission);
        return check == PackageManager.PERMISSION_GRANTED;
    }
}
