package com.stormkern.arttrackplus.db.taskparams;

import android.support.annotation.NonNull;

import com.raizlabs.android.dbflow.sql.builder.Condition;
import com.raizlabs.android.dbflow.sql.language.OrderBy;

/**
 * Created by StormKern on 11/13/2015.
 */
public class DBTaskParams {

    public Condition[] conditions;
    public OrderBy orderBy;

    public DBTaskParams() {
        conditions = new Condition[]{};
        orderBy = null;
    }

    public DBTaskParams(@NonNull Condition[] conditions) {
        this.conditions = conditions;
        orderBy = null;
    }

    public DBTaskParams(@NonNull Condition[] conditions, OrderBy orderBy) {
        this.conditions = conditions;
        this.orderBy = orderBy;
    }
}
