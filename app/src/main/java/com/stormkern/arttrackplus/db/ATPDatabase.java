package com.stormkern.arttrackplus.db;

import com.raizlabs.android.dbflow.annotation.Database;
import com.stormkern.arttrackplus.BuildConfig;

/**
 * Created by StormKern on 11/13/2015.
 */
@Database(name = ATPDatabase.NAME, version = ATPDatabase.VERSION)
public class ATPDatabase {

    public static final String NAME = BuildConfig.DATABASE_NAME;

    public static final int VERSION = BuildConfig.DATABASE_VERSION;

}
