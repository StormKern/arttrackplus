package com.stormkern.arttrackplus.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.runtime.TransactionManager;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.stormkern.arttrackplus.R;
import com.stormkern.arttrackplus.data.Artist;
import com.stormkern.arttrackplus.db.ATPDatabase;
import com.stormkern.arttrackplus.util.Common;
import com.stormkern.arttrackplus.util.date.DateUtils;
import com.stormkern.arttrackplus.util.dialog.DialogUtils;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Generates random test data
 * <p/>
 * Created by StormKern on 3/13/2015.
 */
public class GenerateRandomUsersTask extends AsyncTaskBase<Object, Object, Boolean> {

    public interface Listener {
        void taskCompleted();

        void taskFailed();
    }

    private Context context;
    private Listener listener;
    private int numberOfUsersToGenerate;

    private ProgressDialog progressDialog;

    public GenerateRandomUsersTask(Context context, Listener listener, int numberOfUsersToGenerate) {
        this.context = context;
        this.listener = listener;
        this.numberOfUsersToGenerate = numberOfUsersToGenerate;

        if (this.context != null) {
            progressDialog = new ProgressDialog(this.context);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(context.getString(R.string.generatingRandomArtists, numberOfUsersToGenerate));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        DialogUtils.showDialog(progressDialog);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onPostExecute(Boolean successful) {
        super.onPostExecute(successful);
        DialogUtils.dismissDialog(progressDialog);
        if (listener != null) {
            if (successful) {
                listener.taskCompleted();
            } else {
                listener.taskFailed();
            }
        }
    }

    @Override
    protected Boolean doInBackground(Object... params) {
        boolean successful;
        long start = System.currentTimeMillis();

        SQLiteDatabase db = FlowManager.getDatabase(ATPDatabase.NAME).getWritableDatabase();
        db.beginTransaction();

        try {

            Delete.table(Artist.class);

            Artist artist;

            List<Artist> artistList = new ArrayList<>();

            for (int i = 0; i < numberOfUsersToGenerate; i++) {
                artist = new Artist();
                artist.uuid = UUID.randomUUID().toString();
                artist.realName = RandomStringUtils.randomAlphabetic(15);
                artist.badgeName = RandomStringUtils.randomAlphabetic(10);
                artist.badgeNumber = i + 1;

                artist.enabled = true;
                artist.lotteryEligible = true;

                artistList.add(artist);
            }

            TransactionManager.getInstance().saveOnSaveQueue(artistList);

            db.setTransactionSuccessful();

            successful = true;

        } catch (Exception ex) {
            Log.e(Common.TAG, "Could not generate random artists", ex);
            successful = false;
        }

        db.endTransaction();

        long end = System.currentTimeMillis();
        Log.d(Common.TAG, "Generated " + numberOfUsersToGenerate + " artists in: " + DateUtils.humanizeTimeDuration(end - start));

        return successful;
    }
}
