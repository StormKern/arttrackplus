package com.stormkern.arttrackplus.task.db;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.stormkern.arttrackplus.R;
import com.stormkern.arttrackplus.data.Entity;
import com.stormkern.arttrackplus.task.AsyncTaskBase;
import com.stormkern.arttrackplus.util.Common;
import com.stormkern.arttrackplus.util.dialog.DialogUtils;

import java.lang.ref.SoftReference;

/**
 * Created by StormKern on 11/14/2015.
 */
public class CreateObjectDBTask<T extends Entity> extends AsyncTaskBase<T, Object, T> {


    public interface Listener<T> {
        void taskCompleted(T entity);

        void taskFailed();
    }

    private SoftReference<Context> softContext;
    private boolean showProgress;
    private Listener listener;

    private ProgressDialog progressDialog;

    public CreateObjectDBTask(Context context, boolean showProgress, Listener listener) {
        this.softContext = new SoftReference<>(context);
        this.showProgress = showProgress;
        this.listener = listener;

        if (this.softContext.get() != null) {
            progressDialog = new ProgressDialog(softContext.get());
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setIcon(android.R.drawable.ic_menu_add);
            progressDialog.setTitle(R.string.create);
            progressDialog.setMessage(context.getString(R.string.creatingItem));
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (showProgress) DialogUtils.showDialog(progressDialog);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    protected void onPostExecute(T result) {
        super.onPostExecute(result);
        if (showProgress) DialogUtils.dismissDialog(progressDialog);
        if (listener != null) {
            if (result != null) {
                listener.taskCompleted(result);
            } else {
                listener.taskFailed();
            }
        }

        softContext.clear();
    }

    @SafeVarargs
    @Override
    protected final T doInBackground(T... params) {

        if (softContext.get() == null) return null;

        if (params.length != 1) return null;

        try {

            T entity = params[0];

            entity.insert();

            return entity;

        } catch (Exception ex) {
            Log.e(Common.TAG, "Unable to create database object", ex);
            return null;
        }
    }
}
