package com.stormkern.arttrackplus.task;

import android.app.Activity;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

/**
 * A global task manager that keeps track of any tasks actively running anywhere in the application.
 * Typical use is when you have long running background tasks that require database interaction and
 * don't block the UI thread.
 * <p/>
 * If you have a safe way to terminate your application, say via the back button on your main activity,
 * you would call {@link #hasActiveTasks()} and then decide if you want to cancel any active tasks
 * {@link #cancelActiveTasks(boolean)} or wait until they all complete before closing your database
 * connection by periodically polling {@link #hasActiveTasks()}
 * <p/>
 * Ideally you should keep track of your active {@link AsyncTask}s in your {@link android.app.Activity}
 * and then on {@link Activity#onDestroy()} handle your tasks accordingly.
 * <p/>
 * Created by StormKern on 8/26/2015.
 */
public class AsyncTaskManager {
    private static List<AsyncTask> mActiveTasks;
    private static AsyncTaskManager mInstance;

    /**
     * Gets the current instance of the manager.  If one does not exist, one will be created.
     *
     * @return The current instance of the manager.
     */
    public synchronized static AsyncTaskManager getInstance() {
        if (mInstance == null) mInstance = new AsyncTaskManager();

        return mInstance;
    }

    private AsyncTaskManager() {
        mActiveTasks = new ArrayList<AsyncTask>();
    }

    /**
     * Adds an {@link AsyncTask} to the manager
     *
     * @param task The {@link AsyncTask} to add
     */
    public void addTask(AsyncTask task) {
        mActiveTasks.add(task);
    }

    /**
     * Removes an {@link AsyncTask} from the manager
     *
     * @param task The {@link AsyncTask} to remove
     */
    public void removeTask(AsyncTask task) {
        mActiveTasks.remove(task);
    }

    /**
     * Checks to see if there are any active {@link AsyncTask}s running or are scheduled to run
     * <p/>
     * See {@link AsyncTask#execute(Object[])}, {@link AsyncTask#executeOnExecutor(Executor, Object[])}
     *
     * @return {@code true} if there any active tasks running or scheduled to run; {@code false} otherwise.
     */
    public boolean hasActiveTasks() {
        return !mActiveTasks.isEmpty();
    }

    /**
     * Cancels all active or scheduled {@link AsyncTask}s.
     *
     * @param mayInterruptIfRunning {@code true} if the thread executing this
     *                              task should be interrupted; otherwise, in-progress tasks are allowed
     *                              to complete.
     */
    public void cancelActiveTasks(boolean mayInterruptIfRunning) {
        for (AsyncTask task : mActiveTasks) task.cancel(mayInterruptIfRunning);
    }
}
