package com.stormkern.arttrackplus.task;

import android.os.AsyncTask;
import android.support.annotation.CallSuper;

/**
 * Created by StormKern on 8/26/2015.
 */
public abstract class AsyncTaskBase<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    @Override
    @CallSuper
    protected void onPreExecute() {
        super.onPreExecute();
        AsyncTaskManager manager = AsyncTaskManager.getInstance();
        manager.addTask(this);
    }

    @Override
    @CallSuper
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);
        AsyncTaskManager manager = AsyncTaskManager.getInstance();
        manager.removeTask(this);
    }

    @Override
    @CallSuper
    protected void onCancelled(Result result) {
        super.onCancelled(result);
        AsyncTaskManager manager = AsyncTaskManager.getInstance();
        manager.removeTask(this);
    }
}
