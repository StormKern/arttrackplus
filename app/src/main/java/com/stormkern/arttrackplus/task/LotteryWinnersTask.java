package com.stormkern.arttrackplus.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.runtime.TransactionManager;
import com.raizlabs.android.dbflow.sql.builder.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.stormkern.arttrackplus.R;
import com.stormkern.arttrackplus.data.Artist;
import com.stormkern.arttrackplus.data.Artist$Table;
import com.stormkern.arttrackplus.db.ATPDatabase;
import com.stormkern.arttrackplus.util.Common;
import com.stormkern.arttrackplus.util.date.DateUtils;
import com.stormkern.arttrackplus.util.dialog.DialogUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Runs the Lottery
 * <p/>
 * Created by StormKern on 3/12/2015.
 */
public class LotteryWinnersTask extends AsyncTaskBase<Object, Object, Boolean> {

    public interface Listener {
        /**
         * The task completed
         */
        void taskCompleted();

        /**
         * The task errored out
         */
        void taskFailed();
    }

    private Context context;
    private Listener listener;
    private int numberOfSeats;

    private ProgressDialog progressDialog;

    /**
     * Creates a new LotteryWinnersTask
     *
     * @param context       The context in which to run
     * @param numberOfSeats The number of seats in the artist's alley
     * @param listener      The listener to use
     */
    public LotteryWinnersTask(Context context, int numberOfSeats, Listener listener) {
        this.context = context;
        this.listener = listener;
        this.numberOfSeats = numberOfSeats;

        if (this.context != null) {
            progressDialog = new ProgressDialog(this.context);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(context.getString(R.string.determiningLotteryWinners));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        DialogUtils.showDialog(progressDialog);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onPostExecute(Boolean successful) {
        super.onPostExecute(successful);
        DialogUtils.dismissDialog(progressDialog);
        if (listener != null) {
            if (successful) {
                listener.taskCompleted();
            } else {
                listener.taskFailed();
            }
        }
    }

    @Override
    protected Boolean doInBackground(Object... params) {

        boolean successful;
        long start = System.currentTimeMillis();

        SQLiteDatabase db = FlowManager.getDatabase(ATPDatabase.NAME).getWritableDatabase();
        db.beginTransaction();

        try {

            List<Artist> results = new ArrayList<>();

            //reset the current lists
            List<Artist> allArtists = new Select().from(Artist.class).where(Condition.column(Artist$Table.ENABLED).is(true)).queryList();
            for (Artist artist : allArtists) {
                artist.lottery = false;
                artist.lotteryOrder = 0;
                artist.standby = false;
                artist.standbyOrder = 0;
                artist.signedIn = false;
            }

            TransactionManager.getInstance().saveOnSaveQueue(allArtists);

            List<Artist> eligibleArtists = new Select().from(Artist.class)
                                                .where(Condition.column(Artist$Table.ENABLED).is(true))
                                                .and(Condition.column(Artist$Table.LOTTERYELIGIBLE).is(true))
                                                .queryList();

            List<Artist> guaranteedArtists = new Select().from(Artist.class)
                                                    .where(Condition.column(Artist$Table.LOTTERYGUARANTEED).is(true))
                                                    .queryList();

            int lotteryOrder = 1;
            int standbyOrder = 1;

            int numberOfUsers = eligibleArtists.size();

            //process users that are guaranteed a table
            for (Artist artist : guaranteedArtists) {
                artist.lottery = true;
                artist.lotteryOrder = lotteryOrder;
                results.add(artist);

                lotteryOrder++;
                numberOfUsers--;
                numberOfSeats--;
            }

            //randomly choose the rest
            Random random = new Random();
            Artist rArtist;

            while (numberOfUsers > 0) {
                rArtist = eligibleArtists.get(random.nextInt(eligibleArtists.size()));

                if (!results.contains(rArtist)) {
                    for (Artist artist : eligibleArtists) {
                        if (artist.equals(rArtist)) {
                            if (numberOfSeats > 0) {
                                artist.lottery = true;
                                artist.lotteryOrder = lotteryOrder;
                                lotteryOrder++;
                            } else {
                                artist.standby = true;
                                artist.standbyOrder = standbyOrder;
                                standbyOrder++;
                            }
                            results.add(rArtist);
                            numberOfSeats--;
                            numberOfUsers--;
                        }
                    }
                }
            }

            TransactionManager.getInstance().saveOnSaveQueue(results);

            db.setTransactionSuccessful();

            successful = true;

        } catch (Exception ex) {
            Log.e(Common.TAG, "Could not run LotteryWinnersTask", ex);
            successful = false;
        }

        db.endTransaction();

        long end = System.currentTimeMillis();
        Log.d(Common.TAG, "Completed LotteryTask in: " + DateUtils.humanizeTimeDuration(end - start));

        return successful;
    }
}
