package com.stormkern.arttrackplus.task.db;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.raizlabs.android.dbflow.config.FlowManager;
import com.stormkern.arttrackplus.R;
import com.stormkern.arttrackplus.db.ATPDatabase;
import com.stormkern.arttrackplus.task.AsyncTaskBase;
import com.stormkern.arttrackplus.util.Common;
import com.stormkern.arttrackplus.util.date.DateUtils;
import com.stormkern.arttrackplus.util.dialog.DialogUtils;

import java.lang.ref.SoftReference;

/**
 * Resets the database to initial install
 * <p/>
 * Created by StormKern on 5/13/2015.
 */
public class DBResetTask extends AsyncTaskBase<Object, Object, Boolean> {

    public interface Listener {
        void taskCompleted();

        void taskFailed();
    }

    private SoftReference<Context> softContext;
    private ProgressDialog progressDialog;
    private Listener listener;

    public DBResetTask(Context context, Listener listener) {
        this.softContext = new SoftReference<>(context);
        this.listener = listener;


        if (this.softContext.get() != null) {
            progressDialog = new ProgressDialog(this.softContext.get());
            progressDialog.setTitle(R.string.resetDatabase);
            progressDialog.setMessage(context.getString(R.string.resettingDatabase));
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        DialogUtils.showDialog(progressDialog);
    }

    @Override
    protected void onPostExecute(Boolean successful) {
        super.onPostExecute(successful);
        DialogUtils.dismissDialog(progressDialog);

        if (listener != null) {
            if (successful) {
                listener.taskCompleted();
            } else {
                listener.taskFailed();
            }
        }

        softContext.clear();
    }

    @Override
    protected Boolean doInBackground(Object... params) {
        if (softContext.get() == null) return false;

        long start = System.currentTimeMillis();
        long end;

        boolean successful;

        try {

            FlowManager.getDatabase(ATPDatabase.NAME).reset(softContext.get());


            end = System.currentTimeMillis();

            successful = true;
        } catch (Exception ex) {
            Log.e(Common.TAG, "Could not perform DBResetTask", ex);

            end = System.currentTimeMillis();
            successful = false;
        }

        Log.d(Common.TAG, "Completed DBResetTask in: " + DateUtils.humanizeTimeDuration(end - start));

        return successful;
    }
}
