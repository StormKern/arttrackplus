package com.stormkern.arttrackplus.task;

/**
 * Thrown when an error occurs in an {@link android.os.AsyncTask}
 * <p/>
 * Created by StormKern on 3/13/2015.
 */
public class TaskException extends RuntimeException {
    public TaskException() {
    }

    public TaskException(String detailMessage) {
        super(detailMessage);
    }

    public TaskException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public TaskException(Throwable throwable) {
        super(throwable);
    }
}
