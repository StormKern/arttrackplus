package com.stormkern.arttrackplus.task.db;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.stormkern.arttrackplus.R;
import com.stormkern.arttrackplus.data.Entity;
import com.stormkern.arttrackplus.task.AsyncTaskBase;
import com.stormkern.arttrackplus.util.Common;
import com.stormkern.arttrackplus.util.dialog.DialogUtils;

import java.lang.ref.SoftReference;

/**
 * Created by StormKern on 11/14/2015.
 */
public class DBObjectTask extends AsyncTaskBase<Entity, Object, Object> {

    public interface DBObjectTaskListener {
        void taskCompleted();
    }

    // available tasks
    public static final int TASK_UPDATE_OBJECT = 1;
    public static final int TASK_DELETE_OBJECT = 2;

    private SoftReference<Context> softContext;
    private DBObjectTaskListener listener;
    private ProgressDialog progressDialog;
    private int task;
    private boolean showProgress;
    private boolean updateChildren = true;

    public DBObjectTask(Context context, DBObjectTaskListener listener, int task, boolean showProgress, boolean updateChildren) {
        this.softContext = new SoftReference<>(context);
        this.listener = listener;
        this.task = task;
        this.showProgress = showProgress;
        this.updateChildren = updateChildren;

        if (this.softContext.get() != null && showProgress) {
            progressDialog = new ProgressDialog(this.softContext.get());
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);

            if (this.task == TASK_DELETE_OBJECT) {
                progressDialog.setIcon(android.R.drawable.ic_menu_delete);
                progressDialog.setTitle(R.string.delete);
                progressDialog.setMessage(context.getString(R.string.deletingItem));
            } else {
                progressDialog.setIcon(android.R.drawable.ic_menu_save);
                progressDialog.setTitle(R.string.update);
                progressDialog.setMessage(context.getString(R.string.updatingItem));
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (showProgress) DialogUtils.showDialog(progressDialog);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (showProgress) DialogUtils.dismissDialog(progressDialog);
        if (listener != null) listener.taskCompleted();

        softContext.clear();
    }

    @Override
    protected Object doInBackground(Entity... params) {

        if (softContext.get() == null) return null;

        try {

            if (params.length != 1) return null;

            Entity entity = params[0];

            if (task == TASK_DELETE_OBJECT) {
                entity.delete();
            } else {
                entity.save();
            }

        } catch (Exception ex) {
            Log.e(Common.TAG, "Could not perform DBObjectTask", ex);
        }

        return null;
    }
}