package com.stormkern.arttrackplus.task.db;

import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.util.Log;

import com.raizlabs.android.dbflow.sql.language.Select;
import com.stormkern.arttrackplus.R;
import com.stormkern.arttrackplus.data.Artist;
import com.stormkern.arttrackplus.data.Day;
import com.stormkern.arttrackplus.task.AsyncTaskBase;
import com.stormkern.arttrackplus.util.Common;
import com.stormkern.arttrackplus.util.date.DateUtils;
import com.stormkern.arttrackplus.util.dialog.DialogUtils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileWriter;
import java.lang.ref.SoftReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * Exports the contexts of the database to a csv stored at {@link Common#exportPath}
 * <p/>
 * Created by StormKern on 5/15/2015.
 */
public class DBExportToCSVTask extends AsyncTaskBase<Object, Object, Boolean> {

    public interface Listener {
        void taskCompleted();

        void taskFailed();
    }

    private SoftReference<Context> softContext;
    private Listener listener;

    private File exportFile;
    private File exportDir;

    private ProgressDialog progressDialog;

    public DBExportToCSVTask(Context context, Listener listener) {
        this.softContext = new SoftReference<>(context);
        this.listener = listener;

        String basePath = Common.exportPath;
        String fileName = "ArtTrackPlus-export-" + getTimeStamp() + ".csv";

        exportDir = new File(basePath);
        exportFile = new File(basePath, fileName);

        if (this.softContext.get() != null) {
            progressDialog = new ProgressDialog(this.softContext.get());
            progressDialog.setTitle(R.string.exporting);
            progressDialog.setMessage(context.getString(R.string.exportingDBToCSV));
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        DialogUtils.showDialog(progressDialog);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onPostExecute(Boolean successful) {
        super.onPostExecute(successful);
        DialogUtils.dismissDialog(progressDialog);
        if (listener != null) {
            if (successful) {
                if (softContext.get() != null) MediaScannerConnection.scanFile(softContext.get(), new String[]{exportDir.getAbsolutePath()}, null, null);
                listener.taskCompleted();
            } else {
                listener.taskFailed();
            }
        }

        softContext.clear();
    }

    @Override
    protected Boolean doInBackground(Object... params) {
        if (softContext.get() == null) return false;

        boolean successful;
        long start = System.currentTimeMillis();

        try {
            if (exportFile.exists()) exportFile.delete();

            if (!exportFile.createNewFile()) {
                Log.e(Common.TAG, "Failed to create new file");
                return false;
            }

            CSVWriter writer = new CSVWriter(new FileWriter(exportFile));
            List<String[]> csvData = new ArrayList<>();
            csvData.add(new String[]{
                            "UUID",
                            "Enabled",
                            "Badge Name",
                            "Badge Number",
                            "Real Name",
                            "Email",
                            "Phone Number",
                            "Remarks",
                            "Signed In",
                            "Standby",
                            "Standby Order",
                            "Lottery",
                            "Lottery Order",
                            "Lottery Eligible",
                            "Lottery Guaranteed",
                            "Last Signed In Date",
                            "Seated Table",
                            "Checked-in Days",
                            "Standby Days"
                    }
            );


            SimpleDateFormat fullDate = new SimpleDateFormat("EEE, d MMM yyyy hh:mm:a", Locale.getDefault());

            List<Artist> artists = new Select().from(Artist.class).queryList();

            String checkedInDays;
            String standByDays;
            for (Artist artist : artists) {

                standByDays = getDaysString(artist.getStandByDays());
                checkedInDays = getDaysString(artist.getCheckedInDays());


                csvData.add(new String[]{
                                artist.uuid,
                                artist.enabled != null ? artist.enabled + "" : "FALSE",
                                artist.badgeName,
                                artist.badgeNumber + "",
                                artist.realName,
                                artist.email,
                                artist.phoneNumber,
                                artist.remarks,
                                artist.signedIn != null ? artist.signedIn + "" : "FALSE",
                                artist.standby != null ? artist.standby + "" : "FALSE",
                                artist.standbyOrder != null ? artist.standbyOrder + "" : "",
                                artist.lottery != null ? artist.lottery + "" : "FALSE",
                                artist.lotteryOrder != null ? artist.lotteryOrder + "" : "",
                                artist.lotteryEligible != null ? artist.lotteryEligible + "" : "FALSE",
                                artist.lotteryGuaranteed != null ? artist.lotteryGuaranteed + "" : "FALSE",
                                artist.signInDate != null ? fullDate.format(artist.signInDate) : "NEVER",
                                artist.seatedTable != null ? artist.seatedTable + "" : "NONE",
                                checkedInDays,
                                standByDays
                        }
                );
            }

            writer.writeAll(csvData);
            writer.close();

            successful = true;

            String dataDir = softContext.get().getApplicationInfo().dataDir;

            try{
                File databaseDir = new File(dataDir + File.separator + "databases");
                Log.v(Common.TAG, Arrays.toString(databaseDir.listFiles()));

                FileUtils.copyDirectory(databaseDir, exportDir);
            }catch (Exception ex){
                Log.e(Common.TAG, "Could not copy databases folder", ex);
            }


        } catch (Exception ex) {
            Log.e(Common.TAG, "Could not export database to csv file", ex);
            successful = false;
        }

        long end = System.currentTimeMillis();

        Log.d(Common.TAG, "Exported database to CSV in: " + DateUtils.humanizeTimeDuration(end - start));

        return successful;
    }

    private String getTimeStamp() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return simpleDateFormat.format(new Date());
    }

    private String getDaysString(List<? extends Day> days) {
        SimpleDateFormat dayOfWeek = new SimpleDateFormat("EEEE", Locale.getDefault());

        String daysString;

        if (days.isEmpty()) {
            daysString = "[NONE]";
        } else {
            daysString = "[";
            int count = 0;
            for (Day day : days) {
                daysString += dayOfWeek.format(day.date);
                if (count < days.size()) daysString += ", ";
            }
            daysString += "]";
        }

        return daysString;
    }
}
