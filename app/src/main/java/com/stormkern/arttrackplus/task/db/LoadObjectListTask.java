package com.stormkern.arttrackplus.task.db;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.From;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.sql.language.Where;
import com.stormkern.arttrackplus.R;
import com.stormkern.arttrackplus.db.ATPDatabase;
import com.stormkern.arttrackplus.db.taskparams.DBTaskParams;
import com.stormkern.arttrackplus.task.AsyncTaskBase;
import com.stormkern.arttrackplus.task.TaskException;
import com.stormkern.arttrackplus.util.Common;
import com.stormkern.arttrackplus.util.date.DateUtils;
import com.stormkern.arttrackplus.util.dialog.DialogUtils;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by StormKern on 11/13/2015.
 */
public class LoadObjectListTask<T> extends AsyncTaskBase<DBTaskParams, Object, List<T>> {

    public interface LoadObjectListTaskListener<T> {
        void objectListLoaded(List<T> list);
    }

    private SoftReference<Context> softContext;
    private LoadObjectListTaskListener Listener;
    private Class type;
    private boolean showProgress;
    private ProgressDialog progressDialog;

    public LoadObjectListTask(Context context, LoadObjectListTaskListener Listener, Class type, boolean showProgress) {
        this.softContext = new SoftReference<>(context);
        this.Listener = Listener;
        this.type = type;
        this.showProgress = showProgress;

        if (this.softContext.get() != null && showProgress) {
            progressDialog = new ProgressDialog(this.softContext.get());
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setTitle(R.string.load);
            progressDialog.setMessage(context.getString(R.string.loading));
            progressDialog.setIcon(android.R.drawable.ic_menu_search);
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (showProgress) DialogUtils.showDialog(progressDialog);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void onPostExecute(List<T> ts) {
        super.onPostExecute(ts);
        if (showProgress) DialogUtils.dismissDialog(progressDialog);
        if (Listener != null) Listener.objectListLoaded(ts);

        softContext.clear();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected List<T> doInBackground(DBTaskParams... params) {

        if (softContext.get() == null) return new ArrayList<>();

        if (params.length != 1) throw new TaskException("Invalid parameters for list task");

        SQLiteDatabase db = FlowManager.getDatabase(ATPDatabase.NAME).getWritableDatabase();
        db.beginTransaction();

        List<T> returnList = new ArrayList<>();

        try {
            DBTaskParams taskParams = params[0];


            long start = System.currentTimeMillis();

            Select select = new Select();
            From from = select.from(type);
            Where where = from.where(taskParams.conditions);
            if (taskParams.orderBy != null) {
                where = where.orderBy(taskParams.orderBy);
            }

            returnList = where.queryList();

            long end = System.currentTimeMillis();

            Log.d(Common.TAG, "Queried " + type.getSimpleName() + " in " + DateUtils.humanizeTimeDuration(end - start));

        } catch (Exception ex) {
            Log.e(Common.TAG, "Unable to load object list", ex);
        }

        db.endTransaction();

        return returnList;
    }
}
