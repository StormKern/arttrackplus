package com.stormkern.arttrackplus.app.fragment.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import java.util.Map;

/**
 * Created by StormKern on 5/13/2015.
 */
public abstract class SettingsBaseFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    // the resource id of the preferences to display
    private int preferencesId;

    /**
     * Constructs a base preference fragment to display a preference file.
     *
     * @param prefId The resource id of the preference file.
     */
    SettingsBaseFragment(int prefId) {
        preferencesId = prefId;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set the default values and load our preferences
        PreferenceManager.setDefaultValues(getActivity(), preferencesId, true);
        addPreferencesFromResource(preferencesId);

    }

    @Override
    public void onStart() {
        super.onStart();

        // register to receive user changes
        PreferenceManager mg = this.getPreferenceManager();
        SharedPreferences prefs = mg.getSharedPreferences();
        prefs.registerOnSharedPreferenceChangeListener(this);

        // update the displayed values
        updateSummaryValues(prefs);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // unregister our change listener
        PreferenceManager mg = this.getPreferenceManager();
        SharedPreferences prefs = mg.getSharedPreferences();
        prefs.unregisterOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        // update the summary if the preference is of list type
        // to display the current name
        Preference p = findPreference(key);
        if (p instanceof ListPreference) {
            p.setSummary(((ListPreference) p).getEntry());

        } else if (p instanceof EditTextPreference) {
            p.setSummary(((EditTextPreference) p).getText());
        }

    }

    /**
     * This method updates the "Summary" displayed for list preferences. The
     * summary name will be set to the preference entry.
     *
     * @param prefs The preference collection to iterate
     */
    private void updateSummaryValues(SharedPreferences prefs) {

        Map<String, ?> map = prefs.getAll();
        for (Map.Entry<String, ?> e : map.entrySet()) {
            Preference p = findPreference(e.getKey());
            if (p instanceof ListPreference) {
                ListPreference lp = (ListPreference) p;
                if (lp.getEntry() != null) {
                    p.setSummary(lp.getEntry());
                }
            } else {
                if (p != null && p.getTitle() != null) p.setSummary(p.getTitle());
            }
        }

    }
}
