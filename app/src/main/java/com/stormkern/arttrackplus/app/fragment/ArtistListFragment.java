package com.stormkern.arttrackplus.app.fragment;

import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.raizlabs.android.dbflow.sql.builder.Condition;
import com.stormkern.arttrackplus.R;
import com.stormkern.arttrackplus.app.adaptor.ArtistListRecyclerAdapter;
import com.stormkern.arttrackplus.app.widget.swipe.util.SwipeAttributes;
import com.stormkern.arttrackplus.app.widget.swipe.util.DividerItemDecoration;
import com.stormkern.arttrackplus.data.Artist;
import com.stormkern.arttrackplus.data.Artist$Table;
import com.stormkern.arttrackplus.db.taskparams.DBTaskParams;
import com.stormkern.arttrackplus.task.TaskException;
import com.stormkern.arttrackplus.task.db.LoadObjectListTask;

import java.util.List;

/**
 * Created by StormKern on 3/13/2015.
 */
public class ArtistListFragment extends Fragment {

    public enum Type {
        ALL("All Artists"),
        LOTTERY("Lottery"),
        STANDBY("Standby"),
        SEATED("Seated"),
        GUARANTEED("Lottery Guaranteed");

        private final String name;

        Type(final String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    private static final String ARG_SECTION_NAME = "section_name";

    private Type type;

    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;

    public static ArtistListFragment newInstance(Type type) {
        ArtistListFragment fragment = new ArtistListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NAME, type.ordinal());
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        type = Type.values()[getArguments().getInt(ARG_SECTION_NAME)];

        View rootView = inflater.inflate(R.layout.fragment_artist_list, container, false);

        refreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadArtistList(false);
            }
        });

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        boolean canSupportMultipleColumns = getResources().getBoolean(R.bool.canSupportMultipleColumns);

        if (canSupportMultipleColumns) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        } else {
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }

        recyclerView.addItemDecoration(new DividerItemDecoration(getResources().getDrawable(R.drawable.divider)));

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadArtistList(true);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        boolean canSupportMultipleColumns = getResources().getBoolean(R.bool.canSupportMultipleColumns);

        if (canSupportMultipleColumns) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        } else {
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
    }

    private void loadArtistList(boolean showProgress) {

        LoadObjectListTask.LoadObjectListTaskListener<Artist> listener2 = new LoadObjectListTask.LoadObjectListTaskListener<Artist>() {
            @Override
            public void objectListLoaded(List<Artist> list) {
                mAdapter = new ArtistListRecyclerAdapter(getActivity(), list);

                ((ArtistListRecyclerAdapter) mAdapter).setMode(SwipeAttributes.Mode.Single);

                recyclerView.setAdapter(mAdapter);

                refreshLayout.setRefreshing(false);
            }
        };

        DBTaskParams taskParams2;

        switch (type) {
            case ALL:
                taskParams2 = new DBTaskParams();
                break;
            case LOTTERY:
                taskParams2 = new DBTaskParams(new Condition[]{Condition.column(Artist$Table.ENABLED).is(true), Condition.column(Artist$Table.LOTTERY).is(true)});
                break;
            case STANDBY:
                taskParams2 = new DBTaskParams(new Condition[]{Condition.column(Artist$Table.ENABLED).is(true), Condition.column(Artist$Table.STANDBY).is(true)});
                break;
            case SEATED:
                taskParams2 = new DBTaskParams(new Condition[]{Condition.column(Artist$Table.ENABLED).is(true), Condition.column(Artist$Table.SIGNEDIN).is(true)});
                break;
            case GUARANTEED:
                taskParams2 = new DBTaskParams(new Condition[]{Condition.column(Artist$Table.ENABLED).is(true), Condition.column(Artist$Table.LOTTERYGUARANTEED).is(true)});
                break;
            default:
                throw new TaskException("Invalid name supplied");
        }

        LoadObjectListTask<Artist> task2 = new LoadObjectListTask<>(getContext(), listener2, Artist.class, showProgress);
        task2.execute(taskParams2);

    }

    public ArtistListRecyclerAdapter getAdapter() {
        return (ArtistListRecyclerAdapter) mAdapter;
    }

    public Type getType() {
        return type;
    }
}
