package com.stormkern.arttrackplus.app.widget.swipe.adaptors;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.stormkern.arttrackplus.app.widget.swipe.util.SwipeAttributes;
import com.stormkern.arttrackplus.app.widget.SwipeLayout;
import com.stormkern.arttrackplus.app.widget.swipe.impl.SwipeItemMangerImpl;
import com.stormkern.arttrackplus.app.widget.swipe.interfaces.SwipeAdapterInterface;
import com.stormkern.arttrackplus.app.widget.swipe.interfaces.SwipeItemMangerInterface;

import java.util.List;

/**
 * Created by StormKern on 5/9/2015.
 */
public abstract class RecyclerSwipeAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> implements SwipeItemMangerInterface, SwipeAdapterInterface {

    public SwipeItemMangerImpl mItemManger = new SwipeItemMangerImpl(this);

    @Override
    public abstract VH onCreateViewHolder(ViewGroup parent, int viewType);

    @Override
    public abstract void onBindViewHolder(VH viewHolder, final int position);

    @Override
    public void notifyDatasetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public void openItem(int position) {
        mItemManger.openItem(position);
    }

    @Override
    public void closeItem(int position) {
        mItemManger.closeItem(position);
    }

    @Override
    public void closeAllExcept(SwipeLayout layout) {
        mItemManger.closeAllExcept(layout);
    }

    @Override
    public void closeAllItems() {
        mItemManger.closeAllItems();
    }

    @Override
    public List<Integer> getOpenItems() {
        return mItemManger.getOpenItems();
    }

    @Override
    public List<SwipeLayout> getOpenLayouts() {
        return mItemManger.getOpenLayouts();
    }

    @Override
    public void removeShownLayouts(SwipeLayout layout) {
        mItemManger.removeShownLayouts(layout);
    }

    @Override
    public boolean isOpen(int position) {
        return mItemManger.isOpen(position);
    }

    @Override
    public SwipeAttributes.Mode getMode() {
        return mItemManger.getMode();
    }

    @Override
    public void setMode(SwipeAttributes.Mode mode) {
        mItemManger.setMode(mode);
    }
}