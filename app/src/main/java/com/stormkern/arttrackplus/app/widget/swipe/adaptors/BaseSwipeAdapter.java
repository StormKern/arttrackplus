package com.stormkern.arttrackplus.app.widget.swipe.adaptors;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.stormkern.arttrackplus.app.widget.swipe.util.SwipeAttributes;
import com.stormkern.arttrackplus.app.widget.SwipeLayout;
import com.stormkern.arttrackplus.app.widget.swipe.impl.SwipeItemMangerImpl;
import com.stormkern.arttrackplus.app.widget.swipe.interfaces.SwipeAdapterInterface;
import com.stormkern.arttrackplus.app.widget.swipe.interfaces.SwipeItemMangerInterface;

import java.util.List;

/**
 * Created by StormKern on 5/9/2015.
 */
public abstract class BaseSwipeAdapter extends BaseAdapter implements SwipeItemMangerInterface, SwipeAdapterInterface {

    protected SwipeItemMangerImpl mItemManger = new SwipeItemMangerImpl(this);

    /**
     * return the {@link SwipeLayout} resource id, int the view item.
     *
     * @param position
     * @return
     */
    public abstract int getSwipeLayoutResourceId(int position);

    /**
     * generate a new view item.
     * Never bind SwipeListener or fill values here, every item has a chance to fill value or bind
     * listeners in fillValues.
     * to fill it in {@code fillValues} method.
     *
     * @param position
     * @param parent
     * @return
     */
    public abstract View generateView(int position, ViewGroup parent);

    /**
     * fill values or bind listeners to the view.
     *
     * @param position
     * @param convertView
     */
    public abstract void fillValues(int position, View convertView);

    @Override
    public void notifyDatasetChanged() {
        super.notifyDataSetChanged();
    }


    @Override
    public final View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = generateView(position, parent);
        }
        mItemManger.bind(v, position);
        fillValues(position, v);
        return v;
    }

    @Override
    public void openItem(int position) {
        mItemManger.openItem(position);
    }

    @Override
    public void closeItem(int position) {
        mItemManger.closeItem(position);
    }

    @Override
    public void closeAllExcept(SwipeLayout layout) {
        mItemManger.closeAllExcept(layout);
    }

    @Override
    public void closeAllItems() {
        mItemManger.closeAllItems();
    }

    @Override
    public List<Integer> getOpenItems() {
        return mItemManger.getOpenItems();
    }

    @Override
    public List<SwipeLayout> getOpenLayouts() {
        return mItemManger.getOpenLayouts();
    }

    @Override
    public void removeShownLayouts(SwipeLayout layout) {
        mItemManger.removeShownLayouts(layout);
    }

    @Override
    public boolean isOpen(int position) {
        return mItemManger.isOpen(position);
    }

    @Override
    public SwipeAttributes.Mode getMode() {
        return mItemManger.getMode();
    }

    @Override
    public void setMode(SwipeAttributes.Mode mode) {
        mItemManger.setMode(mode);
    }
}