package com.stormkern.arttrackplus.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.stormkern.arttrackplus.R;
import com.stormkern.arttrackplus.data.Artist;
import com.stormkern.arttrackplus.data.CheckedInDay;
import com.stormkern.arttrackplus.data.Day;
import com.stormkern.arttrackplus.data.StandByDay;
import com.stormkern.arttrackplus.util.date.DateUtils;
import com.stormkern.arttrackplus.util.dialog.DialogUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by StormKern on 3/13/2015.
 */
@SuppressWarnings("SpellCheckingInspection")
public class ArtistInformationDialog extends Dialog implements View.OnClickListener, CheckBox.OnCheckedChangeListener {

    public interface ArtistInformationDialogListener {
        void userUpdated(Artist artist);

        void userCanceled();
    }

    private Button btnAddUpdate;
    private Button btnSignInOut;
    private Button btnStandby;

    private TextView tvArtistName;
    private TextView tvLastSeated;

    private EditText etBadgeName;
    private EditText etBadgeNumber;
    private EditText etTableNumber;
    private EditText etPhoneNumber;

    private CheckBox cbLotteryEligible;
    private CheckBox cbLotteryGuaranteed;

    private boolean badgeNameValid = false;
    private boolean badgeNumberValid = false;
    private boolean tableNumberValid = true;
    private boolean phoneNumberValid = true;

    private Artist artist;
    private boolean isNew;
    private ArtistInformationDialogListener listener;

    private DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT);
    private DateFormat dayOfWeekFormat = new SimpleDateFormat("EEEE", Locale.getDefault());

    public ArtistInformationDialog(Context context, Artist artist, boolean isNew, ArtistInformationDialogListener listener) {
        super(context);
        super.setTitle(R.string.artistInformation);

        this.artist = artist;
        this.isNew = isNew;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.dialog_artist_information);
        super.setCancelable(false);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        findViewById(R.id.cancel).setOnClickListener(this);

        tvArtistName = (TextView) findViewById(R.id.tvArtistName);

        btnSignInOut = (Button) findViewById(R.id.btnSignInOut);
        btnSignInOut.setText(artist.signedIn != null && artist.signedIn ? R.string.signOut : R.string.signIn);
        btnSignInOut.setOnClickListener(this);

        btnStandby = (Button) findViewById(R.id.btnStandby);
        btnStandby.setText(artist.standby != null && artist.standby ? R.string.removeStandBy : R.string.putOnStandby);
        btnStandby.setOnClickListener(this);

        tvLastSeated = (TextView) findViewById(R.id.tvLastSeated);
        tvLastSeated.setText(artist.signInDate != null ? dateFormat.format(artist.signInDate) : getContext().getString(R.string.never));

        btnAddUpdate = (Button) findViewById(R.id.addUpdate);
        btnAddUpdate.setText(isNew ? R.string.add : R.string.update);
        btnAddUpdate.setOnClickListener(this);

        populateUI();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel:
                DialogUtils.dismissDialog(this);
                if (this.listener != null) listener.userCanceled();
                break;
            case R.id.addUpdate:
                DialogUtils.dismissDialog(this);
                if (this.listener != null) listener.userUpdated(artist);
                break;
            case R.id.btnSignInOut:
                handleSignInOut();
                break;
            case R.id.btnStandby:
                handleStandBy();
                break;

        }
    }

    private void populateUI() {
        TextView textView;

        etBadgeName = (EditText) findViewById(R.id.etBadgeName);
        if (artist.badgeName != null && !artist.badgeName.isEmpty()) {
            etBadgeName.setText(artist.badgeName);
            tvArtistName.setText(artist.badgeName);

            badgeNameValid = true;
        } else {
            etBadgeName.setError(getContext().getString(R.string.required));
            badgeNameValid = false;
        }
        etBadgeName.setText(artist.badgeName);
        etBadgeName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    artist.badgeName = s.toString();
                    tvArtistName.setText(artist.badgeName);
                    badgeNameValid = true;
                } else {
                    etBadgeName.setError("Required");
                    badgeNameValid = false;
                }
                enableDisableUpdateButton();
            }
        });

        etBadgeNumber = (EditText) findViewById(R.id.etBadgeNumber);
        if (artist.badgeNumber != null) {
            etBadgeNumber.setText(artist.badgeNumber + "");
            badgeNumberValid = true;
        } else {
            etBadgeNumber.setError(getContext().getString(R.string.required));
            badgeNumberValid = false;
        }
        etBadgeNumber.setText(artist.badgeNumber != null ? (artist.badgeNumber + "") : "");
        etBadgeNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    artist.badgeNumber = Integer.parseInt(s.toString());
                    badgeNumberValid = true;
                } else {
                    etBadgeNumber.setError(getContext().getString(R.string.required));
                    artist.badgeNumber = null;
                    badgeNumberValid = false;
                }
                enableDisableUpdateButton();
            }
        });


        if (artist.signedIn != null && artist.signedIn) tableNumberValid = artist.seatedTable != null;

        etTableNumber = (EditText) findViewById(R.id.etTableNumber);
        etTableNumber.setText(artist.seatedTable != null ? artist.seatedTable + "" : "");
        etTableNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty() && artist.signedIn != null && artist.signedIn) {
                    artist.seatedTable = Integer.parseInt(s.toString());
                    tableNumberValid = true;
                } else if (s.toString().isEmpty() && artist.signedIn != null && artist.signedIn) {
                    etTableNumber.setError(getContext().getString(R.string.required));
                    tableNumberValid = false;
                }
                enableDisableUpdateButton();
            }
        });

        etPhoneNumber = (EditText) findViewById(R.id.etPhone);
        etPhoneNumber.setText(artist.phoneNumber != null ? artist.phoneNumber : "");
        etPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    if (s.toString().matches("^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}$")) {
                        artist.phoneNumber = s.toString();
                        phoneNumberValid = true;
                    } else {
                        etPhoneNumber.setError(getContext().getString(R.string.invalidPhoneNumber));
                        phoneNumberValid = false;
                    }
                } else {
                    artist.phoneNumber = null;
                    phoneNumberValid = true;
                }
                enableDisableUpdateButton();
            }
        });

        EditText etRemarks = (EditText) findViewById(R.id.etRemarks);
        etRemarks.setText(artist.remarks);
        etRemarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                artist.remarks = s.toString();
            }
        });

        cbLotteryEligible = (CheckBox) findViewById(R.id.cbEligible);
        cbLotteryEligible.setChecked(artist.lotteryEligible != null && artist.lotteryEligible);
        cbLotteryEligible.setOnCheckedChangeListener(this);

        cbLotteryGuaranteed = (CheckBox) findViewById(R.id.cbGuaranteed);
        cbLotteryGuaranteed.setChecked(artist.lotteryGuaranteed != null && artist.lotteryGuaranteed);
        cbLotteryGuaranteed.setOnCheckedChangeListener(this);

        textView = (TextView) findViewById(R.id.tvDaysSeats);
        setDaysViewText(textView, artist.checkedInDays);

        textView = (TextView) findViewById(R.id.tvStandByDays);
        setDaysViewText(textView, artist.standByDays);

        btnSignInOut.setEnabled(getSignInOutButtonStatus());
        btnStandby.setEnabled(getStandbyButtonStatus());

        enableDisableUpdateButton();

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cbEligible:
                artist.lotteryEligible = isChecked;
                break;
            case R.id.cbGuaranteed:
                artist.lotteryGuaranteed = isChecked;
                break;
        }
    }

    private void handleSignInOut() {
        artist.signedIn = artist.signedIn == null || !artist.signedIn;
        btnSignInOut.setText(artist.signedIn ? R.string.signOut : R.string.signIn);

        if (artist.signedIn) {
            showTableInputDialog();
            artist.signInDate = new Date();
            tvLastSeated.setText(dateFormat.format(artist.signInDate));
        } else {
            DialogInterface.OnClickListener onClickListener = new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    artist.lotteryEligible = which == DialogInterface.BUTTON_POSITIVE;
                    cbLotteryEligible.setChecked(artist.lotteryEligible);
                }
            };

            Dialog dialog = DialogUtils.createYesNoDialog(getContext(), R.string.lottery, R.string.isArtistEligiableTomorrow, onClickListener);
            DialogUtils.showDialog(dialog);
            artist.seatedTable = null;
            etTableNumber.setText("");
        }

        btnStandby.setEnabled(!artist.signedIn);

        Day lastDay;

        if (artist.checkedInDays != null) {
            if (!artist.checkedInDays.isEmpty()) {

                sortDays(artist.checkedInDays);

                lastDay = artist.checkedInDays.get(artist.getCheckedInDays().size() - 1);
                if (!DateUtils.isSameDay(lastDay.date, artist.signInDate)){
                    CheckedInDay day = new CheckedInDay(artist.signInDate);
                    day.artist_uuid = artist.uuid;
                    artist.checkedInDays.add(day);
                }

            } else {
                CheckedInDay day = new CheckedInDay(artist.signInDate);
                day.artist_uuid = artist.uuid;
                artist.checkedInDays.add(day);
            }
        }
    }

    private void handleStandBy() {
        artist.standby = artist.standby == null || !artist.standby;

        btnStandby.setText(artist.standby ? R.string.removeStandBy : R.string.putOnStandby);

        btnSignInOut.setEnabled(!artist.standby);

        if (artist.standby) {
            artist.standByDate = new Date();

            Day lastDay;
            if (artist.standByDays != null) {
                if (!artist.standByDays.isEmpty()) {

                    sortDays(artist.standByDays);

                    lastDay = artist.standByDays.get(artist.getStandByDays().size() - 1);
                    if (!DateUtils.isSameDay(lastDay.date, artist.standByDate)) {
                        StandByDay day = new StandByDay(artist.standByDate);
                        day.artist_uuid = artist.uuid;
                        artist.getStandByDays().add(day);
                    }

                    if (artist.standByDays.size() == 2) {
                        artist.lotteryGuaranteed = true;
                        cbLotteryGuaranteed.setChecked(true);
                    }

                } else {
                    StandByDay day = new StandByDay(artist.standByDate);
                    day.artist_uuid = artist.uuid;

                    artist.standByDays.add(day);
                }
            }
        } else {
            artist.standByDate = null;
        }
    }

     private void sortDays(List<? extends Day> days) {
        Collections.sort(days, new Comparator<Day>() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public int compare(Day lhs, Day rhs) {
                if (lhs.date == null && rhs.date != null) return -1;
                if (lhs.date != null && rhs.date == null) return 1;
                if (lhs.date == null && rhs.date == null) return 0;

                return lhs.date.compareTo(rhs.date);
            }
        });
    }

    private void setDaysViewText(TextView textView, List<? extends Day> days) {
        if (days != null && !days.isEmpty()) {
            StringBuilder daysSeated = new StringBuilder();
            int count = 0;
            for (Day day : days) {
                daysSeated.append(dayOfWeekFormat.format(day.date));
                if (count < days.size() - 1) daysSeated.append(", ");
                count++;
            }

            textView.setText(daysSeated.toString());
        } else {
            textView.setText(R.string.none);
        }
    }

    private boolean getSignInOutButtonStatus() {
        boolean enabled;
        if (artist != null) {
            enabled = artist.standby == null || !artist.standby || (artist.standByDate != null && !DateUtils.isToday(artist.standByDate));

            if (!enabled) enabled = artist.signedIn != null && artist.signInDate != null && !DateUtils.isToday(artist.signInDate);

        } else {
            enabled = false;
        }

        return enabled;
    }

    private boolean getStandbyButtonStatus() {
        boolean enabled;
        if (artist != null) {
            enabled = artist.signedIn != null && (!artist.signedIn || (artist.signInDate != null && !DateUtils.isToday(artist.signInDate)));

            if (!enabled) enabled = artist.standby == null || artist.standByDate != null && DateUtils.isToday(artist.standByDate);

        } else {
            enabled = false;
        }

        return enabled;
    }

    private void showTableInputDialog() {
        NumberInputDialog.Listener listener = new NumberInputDialog.Listener() {
            @Override
            public void numberEntered(int number) {
                artist.seatedTable = number;
                etTableNumber.setText(number + "");
            }

            @Override
            public void canceled() {
                artist.seatedTable = null;
                etTableNumber.setText(null);
            }
        };

        NumberInputDialog numberInputDialog = new NumberInputDialog(getContext(),R.string.tableNumber,R.string.enterTableNumber, listener);
        DialogUtils.showDialog(numberInputDialog);
    }

    private void enableDisableUpdateButton() {
        btnAddUpdate.setEnabled(badgeNameValid && badgeNumberValid && phoneNumberValid && tableNumberValid);
    }
}
