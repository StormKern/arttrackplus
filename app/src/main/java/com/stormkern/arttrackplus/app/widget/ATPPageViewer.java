package com.stormkern.arttrackplus.app.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Provides the same functionality as the standard ViewPager, but allows you to
 * enable/disable swipe pagination.
 * <p/>
 * Created by StormKern on 5/10/2015.
 */
public class ATPPageViewer extends ViewPager {

    private boolean pagingEnabled = true;

    public ATPPageViewer(Context context) {
        super(context);
    }

    public ATPPageViewer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return pagingEnabled && super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return pagingEnabled && super.onTouchEvent(ev);
    }

    public void setPagingEnabled(boolean pagingEnabled) {
        this.pagingEnabled = pagingEnabled;
    }
}
