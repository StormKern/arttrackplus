package com.stormkern.arttrackplus.app.widget.swipe.interfaces;

/**
 * Created by StormKern on 5/9/2015.
 */

public interface SwipeAdapterInterface {

    int getSwipeLayoutResourceId(int position);

    void notifyDatasetChanged();

}