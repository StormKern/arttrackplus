package com.stormkern.arttrackplus.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.stormkern.arttrackplus.R;
import com.stormkern.arttrackplus.util.dialog.DialogUtils;

/**
 * Created by StormKern on 5/16/2015.
 */
public class NumberInputDialog extends Dialog implements View.OnClickListener {

    public interface Listener {
        void numberEntered(int number);

        void canceled();
    }

    private Listener listener;
    private EditText etNumber;

    private String message;

    public NumberInputDialog(Context context, String title, String message, Listener listener) {
        super(context);
        super.setTitle(title);
        super.setCancelable(false);

        this.message = message;
        this.listener = listener;
    }

    public NumberInputDialog(Context context, @StringRes int title, @StringRes int message, Listener listener){
        super(context);
        super.setTitle(title);

        super.setCancelable(false);
        this.message = context.getString(message);
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.dialog_number_input);

        ((TextView) findViewById(R.id.tvMessage)).setText(message);

        etNumber = (EditText) findViewById(R.id.etNumber);

        findViewById(R.id.btnOk).setOnClickListener(this);
        findViewById(R.id.btnCancel).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnOk:
                if (validInput()) {
                    if (listener != null) listener.numberEntered(Integer.parseInt(etNumber.getText().toString().trim()));
                    DialogUtils.dismissDialog(this);
                } else {
                    DialogUtils.showErrorDialog(getContext(), R.string.invalidNumber, R.string.enteredInvalidNumber);
                }
                break;
            case R.id.btnCancel:
                if (listener != null) listener.canceled();
                DialogUtils.dismissDialog(this);
        }

    }

    private boolean validInput() {
        return etNumber.getText() == null || !etNumber.getText().toString().trim().isEmpty();
    }
}
