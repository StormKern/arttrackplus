package com.stormkern.arttrackplus.app.adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.stormkern.arttrackplus.R;
import com.stormkern.arttrackplus.app.adaptor.viewholders.ArtistListViewHolder;
import com.stormkern.arttrackplus.app.dialog.ArtistInformationDialog;
import com.stormkern.arttrackplus.app.widget.swipe.adaptors.RecyclerSwipeAdapter;
import com.stormkern.arttrackplus.data.Artist;
import com.stormkern.arttrackplus.task.db.DBObjectTask;
import com.stormkern.arttrackplus.util.dialog.DialogUtils;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by StormKern on 5/10/2015.
 */
@SuppressWarnings("unused")
public class ArtistListRecyclerAdapter extends RecyclerSwipeAdapter<ArtistListViewHolder> implements ArtistListViewHolder.Listener {

    private Context mContext;
    private List<Artist> mDataSet;
    private List<Artist> mVisibleDataSet;

    public ArtistListRecyclerAdapter(Context context, List<Artist> objects) {
        this.mContext = context;
        this.mDataSet = objects;
        this.mVisibleDataSet = objects;
    }

    @Override
    public ArtistListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_artist_list_item, parent, false);
        return new ArtistListViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(ArtistListViewHolder viewHolder, int position) {
        viewHolder.setArtist(mVisibleDataSet.get(position), mContext);
        mItemManger.bind(viewHolder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return mVisibleDataSet.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    private void showUserDialog(Artist artist, final int position) {
        mItemManger.closeAllItems();
        ArtistInformationDialog.ArtistInformationDialogListener listener = new ArtistInformationDialog.ArtistInformationDialogListener() {
            @Override
            public void userUpdated(Artist user) {
                DBObjectTask.DBObjectTaskListener objectTaskListener = new DBObjectTask.DBObjectTaskListener() {
                    @Override
                    public void taskCompleted() {
                        notifyItemChanged(position);
                    }
                };

                DBObjectTask dbObjectTask = new DBObjectTask(mContext, objectTaskListener, DBObjectTask.TASK_UPDATE_OBJECT, true, true);
                dbObjectTask.execute(user);
            }

            @Override
            public void userCanceled() {
            }
        };

        ArtistInformationDialog dialog = new ArtistInformationDialog(mContext, artist, false, listener);
        DialogUtils.showDialog(dialog);
    }

    public void flushFilter() {
        mVisibleDataSet = new ArrayList<>();
        mVisibleDataSet.addAll(mDataSet);
        notifyDataSetChanged();
    }

    public void setFilter(String queryText) {
        mVisibleDataSet = new ArrayList<>();

        for (Artist artist : mDataSet) {
            if (artist.badgeName.toLowerCase(Locale.getDefault()).contains(queryText) || artist.badgeNumber.toString().contains(queryText)) {
                mVisibleDataSet.add(artist);
            }
        }

        notifyDataSetChanged();
    }

    public void add(Artist artist) {
        mDataSet.add(artist);
        notifyDataSetChanged();
    }

    public void remove(Artist artist) {
        mDataSet.remove(artist);
        notifyDataSetChanged();
    }

    @Override
    public void onViewRecycled(ArtistListViewHolder holder) {
        super.onViewRecycled(holder);
        holder.swipeLayout.close();
    }

    @Override
    public void onViewAttachedToWindow(ArtistListViewHolder holder) {
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public void onViewDetachedFromWindow(ArtistListViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }

    @Override
    public void swipeClicked(View v, Artist artist) {

        int index = mVisibleDataSet.indexOf(artist);

        mItemManger.closeAllItems();

        showUserDialog(artist, index);
    }

    @Override
    public void deleteClicked(final View v, final Artist artist) {

        int index = mDataSet.indexOf(artist);
        if(index >= 0){
            mDataSet.remove(index);
            notifyItemRemoved(index);
        }

        index = mVisibleDataSet.indexOf(artist);
        if(index >= 0){
            mVisibleDataSet.remove(index);
            notifyItemRemoved(index);
        }

        mItemManger.closeAllItems();

        DBObjectTask.DBObjectTaskListener objectTaskListener = new DBObjectTask.DBObjectTaskListener() {
            @Override
            public void taskCompleted() {
                Toast.makeText(v.getContext(), mContext.getString(R.string.toastDeletedArtist, artist.badgeName), Toast.LENGTH_SHORT).show();
            }
        };
        DBObjectTask dbObjectTask = new DBObjectTask(mContext, objectTaskListener, DBObjectTask.TASK_DELETE_OBJECT, false, true);
        dbObjectTask.execute(artist);
    }
}
