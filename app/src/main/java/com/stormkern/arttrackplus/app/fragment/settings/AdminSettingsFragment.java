package com.stormkern.arttrackplus.app.fragment.settings;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.Preference;
import android.text.Html;

import com.stormkern.arttrackplus.R;
import com.stormkern.arttrackplus.app.dialog.NumberInputDialog;
import com.stormkern.arttrackplus.task.GenerateRandomUsersTask;
import com.stormkern.arttrackplus.task.LotteryWinnersTask;
import com.stormkern.arttrackplus.task.db.DBExportToCSVTask;
import com.stormkern.arttrackplus.task.db.DBResetTask;
import com.stormkern.arttrackplus.util.dialog.DialogUtils;
import com.stormkern.arttrackplus.util.permission.PermissionsHelper;

/**
 * Created by StormKern on 5/13/2015.
 */
public class AdminSettingsFragment extends SettingsBaseFragment implements Preference.OnPreferenceClickListener {

    public AdminSettingsFragment() {
        super(R.xml.admin);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        findPreference(getString(R.string.preference_key_run_lottery)).setOnPreferenceClickListener(this);
        findPreference(getString(R.string.preference_key_gen_artists)).setOnPreferenceClickListener(this);
        findPreference(getString(R.string.preference_key_export_database)).setOnPreferenceClickListener(this);
        findPreference(getString(R.string.preference_key_reset_database)).setOnPreferenceClickListener(this);
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {

        if (preference.getKey().equals(getString(R.string.preference_key_run_lottery))) {
            runLottery();
            return true;
        } else if (preference.getKey().equals(getString(R.string.preference_key_gen_artists))) {
            generateTestData();
            return true;
        } else if (preference.getKey().equals(getString(R.string.preference_key_export_database))) {
            exportDatabaseToCSV();
            return true;
        } else if (preference.getKey().equals(getString(R.string.preference_key_reset_database))) {
            resetDatabase();
            return true;
        }

        return false;
    }

    private void runLottery() {
        DialogInterface.OnClickListener yesNotListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    NumberInputDialog.Listener numberInputDialogListener = new NumberInputDialog.Listener() {
                        @Override
                        public void numberEntered(int number) {
                            LotteryWinnersTask.Listener listener = new LotteryWinnersTask.Listener() {
                                @Override
                                public void taskCompleted() { }

                                @Override
                                public void taskFailed() { }
                            };

                            LotteryWinnersTask lotteryWinnersTask = new LotteryWinnersTask(getActivity(), number, listener);
                            lotteryWinnersTask.execute();
                        }

                        @Override
                        public void canceled() { }
                    };
                    NumberInputDialog numberInputDialog = new NumberInputDialog(getActivity(), R.string.lottery, R.string.lotteryNumberOfSeats, numberInputDialogListener);
                    DialogUtils.showDialog(numberInputDialog);
                }
            }
        };
        Dialog yesNoDialog = DialogUtils.createYesNoDialogWithText(getActivity(), getString(R.string.lottery), Html.fromHtml(getString(R.string.lottery_message)), yesNotListener, getString(R.string.proceed), getString(R.string.cancel));
        DialogUtils.showDialog(yesNoDialog);


    }

    private void generateTestData() {

        NumberInputDialog.Listener numberInputDialogListener = new NumberInputDialog.Listener() {
            @Override
            public void numberEntered(int number) {
                GenerateRandomUsersTask task = new GenerateRandomUsersTask(getActivity(), null, number);
                task.execute();
            }

            @Override
            public void canceled() { }
        };
        NumberInputDialog numberInputDialog = new NumberInputDialog(getActivity(), R.string.testData, R.string.enterNumberOfTestArtists, numberInputDialogListener);
        DialogUtils.showDialog(numberInputDialog);


    }

    private void exportDatabaseToCSV() {
        if (PermissionsHelper.hasPermissionGranted(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            DBExportToCSVTask.Listener listener = new DBExportToCSVTask.Listener() {
                @Override
                public void taskCompleted() { }

                @Override
                public void taskFailed() { }
            };

            DBExportToCSVTask dbExportToCSVTask = new DBExportToCSVTask(getActivity(), listener);
            dbExportToCSVTask.execute();
        } else {
            DialogUtils.showErrorDialog(getActivity(), R.string.insufficientPrivileges, R.string.insufficientPrivilegesMessage);
        }
    }

    private void resetDatabase() {

        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    DBResetTask.Listener listener = new DBResetTask.Listener() {
                        @Override
                        public void taskCompleted() { }

                        @Override
                        public void taskFailed() { }
                    };

                    DBResetTask dbResetTask = new DBResetTask(getActivity(), listener);
                    dbResetTask.execute();
                }
            }
        };

        Dialog yesNoDialog = DialogUtils.createYesNoDialog(getActivity(), R.string.resetDatabase, R.string.resetDatabaseMessage, onClickListener);
        DialogUtils.showDialog(yesNoDialog);


    }
}
