package com.stormkern.arttrackplus.app;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.stormkern.arttrackplus.R;
import com.stormkern.arttrackplus.app.dialog.ArtistInformationDialog;
import com.stormkern.arttrackplus.app.fragment.ArtistListFragment;
import com.stormkern.arttrackplus.data.Artist;
import com.stormkern.arttrackplus.app.widget.ATPPageViewer;
import com.stormkern.arttrackplus.task.db.CreateObjectDBTask;
import com.stormkern.arttrackplus.util.dialog.DialogUtils;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ActionBar.TabListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide fragments for each of the
     * three primary sections of the app. We use a {@link android.support.v4.app.FragmentPagerAdapter}
     * derivative, which will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    AppSectionsPagerAdapter mAppSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will display the three primary sections of the app, one at a
     * time.
     */
    ATPPageViewer mViewPager;

    private Fragment currentFragment;
    private MenuItem searchMenuItem;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create the adapter that will return a fragment for each of the three primary sections
        // of the app.
        mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());

        // Set up the action bar.
        final ActionBar actionBar = getSupportActionBar();

        // Specify that the Home/Up button should not be enabled, since there is no hierarchical
        // parent.
        actionBar.setHomeButtonEnabled(false);

        // Specify that we will be displaying tabs in the action bar.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Set up the ViewPager, attaching the adapter and setting up a listener for when the
        // user swipes between sections.
        mViewPager = (ATPPageViewer) findViewById(R.id.pager);
        mViewPager.setPagingEnabled(false);
        mViewPager.setAdapter(mAppSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When swiping between different app sections, select the corresponding tab.
                // We can also use ActionBar.Tab#select() to do this if we have a reference to the
                // Tab.
                actionBar.setSelectedNavigationItem(position);
            }
        });

        //mAppSectionsPagerAdapter.add(new LaunchpadSectionFragment(), "Demo");
        mAppSectionsPagerAdapter.add(ArtistListFragment.newInstance(ArtistListFragment.Type.ALL), ArtistListFragment.Type.ALL.getName());
        mAppSectionsPagerAdapter.add(ArtistListFragment.newInstance(ArtistListFragment.Type.LOTTERY), ArtistListFragment.Type.LOTTERY.getName());
        mAppSectionsPagerAdapter.add(ArtistListFragment.newInstance(ArtistListFragment.Type.SEATED), ArtistListFragment.Type.SEATED.getName());
        mAppSectionsPagerAdapter.add(ArtistListFragment.newInstance(ArtistListFragment.Type.STANDBY), ArtistListFragment.Type.STANDBY.getName());
        mAppSectionsPagerAdapter.add(ArtistListFragment.newInstance(ArtistListFragment.Type.GUARANTEED), ArtistListFragment.Type.GUARANTEED.getName());

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mAppSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by the adapter.
            // Also specify this Activity object, which implements the TabListener interface, as the
            // listener for when this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mAppSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        searchMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
        // Configure the search info and add any event listeners
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(currentFragment instanceof ArtistListFragment){
                    ((ArtistListFragment) currentFragment).getAdapter().setFilter(query);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText.isEmpty()){
                    if(currentFragment instanceof ArtistListFragment){
                        ((ArtistListFragment) currentFragment).getAdapter().flushFilter();
                    }
                }

                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_add_user:
                createNewArtist();
                return true;
            case R.id.action_admin:
                startActivity(new Intent(this, AdminActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {
        // When the given tab is selected, switch to the corresponding page in the ViewPager
        currentFragment = mAppSectionsPagerAdapter.getItem(tab.getPosition());
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {
        //flush filter on previously selected ArtistListFragment
        Fragment previousFragment = mAppSectionsPagerAdapter.getItem(tab.getPosition());
        if(previousFragment instanceof ArtistListFragment) ((ArtistListFragment) previousFragment).getAdapter().flushFilter();

        //close the search menu item
        if(searchMenuItem != null) searchMenuItem.collapseActionView();
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
     * sections of the app.
     */
    public static class AppSectionsPagerAdapter extends FragmentPagerAdapter {

        List<Fragment> mFragments = new ArrayList<Fragment>();
        List<String> mTitles = new ArrayList<String>();

        public AppSectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragments.get(i);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles.get(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        public void add(Fragment fragment, String title) {
            mFragments.add(fragment);
            mTitles.add(title);
            notifyDataSetChanged();
        }
    }

    private void createNewArtist(){
        Artist artist = new Artist();
        artist.enabled = true;

        ArtistInformationDialog.ArtistInformationDialogListener listener = new ArtistInformationDialog.ArtistInformationDialogListener() {
            @Override
            public void userUpdated(Artist user) {
                createArtist(user);
            }

            @Override
            public void userCanceled() { }
        };

        ArtistInformationDialog dialog = new ArtistInformationDialog(this, artist, true, listener);
        DialogUtils.showDialog(dialog);
    }

    private void createArtist(final Artist artist){
        CreateObjectDBTask.Listener<Artist> listener = new CreateObjectDBTask.Listener<Artist>() {
            @Override
            public void taskCompleted(Artist entity) {
                if(currentFragment instanceof ArtistListFragment && ((ArtistListFragment) currentFragment).getType() == ArtistListFragment.Type.ALL){
                    ((ArtistListFragment) currentFragment).getAdapter().add(entity);
                }
            }

            @Override
            public void taskFailed() {
                DialogUtils.showErrorDialog(MainActivity.this, R.string.error, R.string.errorAddingArtist);
            }
        };

        CreateObjectDBTask<Artist> createObjectDBTask = new CreateObjectDBTask<Artist>(MainActivity.this, true, listener);
        createObjectDBTask.execute(artist);
    }
}