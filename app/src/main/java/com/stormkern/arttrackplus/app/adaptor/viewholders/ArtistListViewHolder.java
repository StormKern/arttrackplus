package com.stormkern.arttrackplus.app.adaptor.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.stormkern.arttrackplus.R;
import com.stormkern.arttrackplus.app.widget.SwipeLayout;
import com.stormkern.arttrackplus.data.Artist;

/**
 * Created by StormKern on 5/10/2015.
 */
public class ArtistListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, SwipeLayout.SwipeListener {

    public interface Listener {
        void swipeClicked(View v, Artist artist);

        void deleteClicked(View v, Artist artist);
    }

    private Listener listener;

    public Artist mArtist;

    public SwipeLayout swipeLayout;
    public TextView tvBadgeName;
    public TextView tvBadgeNumber;
    public TextView tvTableNumber;
    public Button buttonDelete;

    public ArtistListViewHolder(View itemView, Listener listener) {
        super(itemView);
        swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
        swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        swipeLayout.setOnClickListener(this);
        swipeLayout.addSwipeListener(this);

        tvBadgeName = (TextView) itemView.findViewById(R.id.tvBadgeName);
        tvBadgeNumber = (TextView) itemView.findViewById(R.id.tvBadgeNumber);
        tvTableNumber = (TextView) itemView.findViewById(R.id.tvTableNumber);

        buttonDelete = (Button) itemView.findViewById(R.id.delete);
        buttonDelete.setOnClickListener(this);

        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if (listener == null) return;

        switch (v.getId()) {
            case R.id.delete:
                swipeLayout.setEnabled(false);
                listener.deleteClicked(v, mArtist);
                break;
            case R.id.swipe:
                listener.swipeClicked(v, mArtist);
        }
    }

    public void setArtist(Artist artist, Context context){
        mArtist = artist;

        tvBadgeName.setText(artist.badgeName);
        tvBadgeNumber.setText(context.getString(R.string.badgeNumberWithSymbol, artist.badgeNumber));

        if (artist.seatedTable != null) {
            tvTableNumber.setVisibility(View.VISIBLE);
            tvTableNumber.setText(context.getString(R.string.tableNumberWithSymbol, artist.seatedTable));
        } else {
            tvTableNumber.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStartOpen(SwipeLayout layout) {
        layout.setEnabled(false);
    }

    @Override
    public void onOpen(SwipeLayout layout) {

    }

    @Override
    public void onStartClose(SwipeLayout layout) {

    }

    @Override
    public void onClose(SwipeLayout layout) {
        layout.setEnabled(true);
    }

    @Override
    public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

    }

    @Override
    public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

    }
}
