package com.stormkern.arttrackplus.app.widget.swipe.interfaces;

import com.stormkern.arttrackplus.app.widget.swipe.util.SwipeAttributes;
import com.stormkern.arttrackplus.app.widget.SwipeLayout;

import java.util.List;

/**
 * Created by StormKern on 5/9/2015.
 */
public interface SwipeItemMangerInterface {

    void openItem(int position);

    void closeItem(int position);

    void closeAllExcept(SwipeLayout layout);

    void closeAllItems();

    List<Integer> getOpenItems();

    List<SwipeLayout> getOpenLayouts();

    void removeShownLayouts(SwipeLayout layout);

    boolean isOpen(int position);

    SwipeAttributes.Mode getMode();

    void setMode(SwipeAttributes.Mode mode);
}
