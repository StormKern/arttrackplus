package com.stormkern.arttrackplus.data;

import com.raizlabs.android.dbflow.annotation.Column;

import java.util.Date;

/**
 * Created by StormKern on 11/13/2015.
 */
public class Day extends Entity {

    public Day() {
    }

    public Day(Date date) {
        this.date = date;
    }

    @Column
    public String artist_uuid;

    @Column
    public Date date;

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "date=" + date + '\'' +
                ", uuid='" + uuid + '\'' +
                '}';
    }
}
