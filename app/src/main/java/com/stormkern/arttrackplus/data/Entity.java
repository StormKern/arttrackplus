package com.stormkern.arttrackplus.data;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.UUID;

/**
 * Created by StormKern on 11/13/2015.
 */
public class Entity extends BaseModel {
    @Column
    @PrimaryKey
    public String uuid;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Entity entity = (Entity) o;

        return new org.apache.commons.lang3.builder.EqualsBuilder()
                .append(uuid, entity.uuid)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37)
                .append(uuid)
                .toHashCode();
    }

    @Override
    public void save() {
        if (uuid == null) {
            uuid = UUID.randomUUID().toString();
            super.insert();
        } else {
            super.save();
        }
    }

    @Override
    public void insert() {
        uuid = UUID.randomUUID().toString();
        super.insert();
    }


    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "uuid='" + uuid + '\'' +
                '}';
    }
}
