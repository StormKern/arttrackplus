package com.stormkern.arttrackplus.data;

import com.raizlabs.android.dbflow.annotation.Table;
import com.stormkern.arttrackplus.db.ATPDatabase;

import java.util.Date;

/**
 * Created by StormKern on 11/13/2015.
 */
@Table(databaseName = ATPDatabase.NAME)
public class StandByDay extends Day {

    public StandByDay() {
    }

    public StandByDay(Date date) {
        super(date);
    }
}
