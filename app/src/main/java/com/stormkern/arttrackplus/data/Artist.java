package com.stormkern.arttrackplus.data;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ModelContainer;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.builder.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.stormkern.arttrackplus.db.ATPDatabase;

import java.util.Date;
import java.util.List;

/**
 * Created by StormKern on 11/13/2015.
 */
@ModelContainer
@Table(databaseName = ATPDatabase.NAME)
public class Artist extends Entity {

    @Column
    public Boolean enabled;

    @Column
    public String realName;

    @Column
    public String badgeName;

    @Column
    public Integer badgeNumber;

    @Column
    public String email;

    @Column
    public String phoneNumber;

    @Column
    public String remarks;

    @Column
    public Boolean signedIn;

    @Column
    public Date signInDate;

    @Column
    public Integer seatedTable;

    @Column
    public Boolean standby;

    @Column
    public Date standByDate;

    @Column
    public Integer standbyOrder;

    @Column
    public Boolean lottery;

    @Column
    public Boolean lotteryEligible;

    @Column
    public Boolean lotteryGuaranteed;

    @Column
    public Integer lotteryOrder;

    public List<CheckedInDay> checkedInDays;

    public List<StandByDay> standByDays;

    @OneToMany(methods = {OneToMany.Method.ALL})
    public List<CheckedInDay> getCheckedInDays(){
        if(checkedInDays == null){
            checkedInDays = new Select().from(CheckedInDay.class).where(Condition.column(CheckedInDay$Table.ARTIST_UUID).is(uuid)).queryList();
        }

        return checkedInDays;
    }

    @OneToMany(methods = {OneToMany.Method.ALL})
    public List<StandByDay> getStandByDays(){
        if(standByDays == null){
            standByDays = new Select().from(StandByDay.class).where(Condition.column(StandByDay$Table.ARTIST_UUID).is(uuid)).queryList();
        }

        return standByDays;
    }

    @Override
    public String toString() {
        return "Artist{" +
                "badgeNumber=" + badgeNumber +
                ", badgeName='" + badgeName + '\'' +
                ", realName='" + realName + '\'' +
                ", uuid='" + uuid + '\'' +
                '}';
    }
}
