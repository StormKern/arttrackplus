package com.stormkern.arttrackplus;

import android.app.Application;
import android.os.Environment;
import android.util.Log;

import com.raizlabs.android.dbflow.config.FlowManager;
import com.stormkern.arttrackplus.util.Common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by StormKern on 5/15/2015.
 */
public class ArtTrackPlusApplication extends Application {

    private static ArtTrackPlusApplication application;

    public ArtTrackPlusApplication() {
        super();
        application = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        FlowManager.init(this);

        String ext = Environment.getExternalStorageDirectory().getAbsolutePath();
        Common.filePath = ext + "/Android/data/" + BuildConfig.APPLICATION_ID + "/files";
        Common.tempPath = Common.filePath + File.separator + "temp";
        Common.userAccessiblePath = ext + File.separator + BuildConfig.USER_ACCESSIBLE_PATH;
        Common.exportPath = Common.userAccessiblePath + File.separator + "export";

        createDirectories();
    }

    public OutputStream getOutputStream(String file) {
        try {
            File f = new File(file);
            if (!f.exists()) {
                f.getParentFile().mkdirs();
                f.createNewFile();
            }

            return new FileOutputStream(file);

        } catch (Exception ex) {
            Log.e(Common.TAG, "Could not get output stream for file: " + file, ex);
            return null;
        }
    }

    public InputStream getInputStream(String file) {
        try {
            File f = new File(file);

            // create the file if it does not exist
            if (!f.exists()) f.createNewFile();

            return new FileInputStream(file);

        } catch (Exception ex) {
            Log.e(Common.TAG, "Could not get input stream for file: " + file, ex);
            return null;
        }

    }

    public void deleteSavedFile(String file) {
        try {
            File f = new File(Common.filePath + File.separator + file);
            if (f.exists()) f.delete();
        } catch (Exception ex) {
            Log.e(Common.TAG, "Could not delete saved file: " + file, ex);
        }
    }

    private void createDirectories() {
        try {
            File f = new File(Common.filePath);
            if (!f.exists()) f.mkdirs();
        } catch (Exception ex) {
            Log.e(Common.TAG, "Could not create main file directory", ex);
        }

        try {
            File f = new File(Common.tempPath);
            if (!f.exists()) f.mkdirs();
        } catch (Exception ex) {
            Log.e(Common.TAG, "Could not create temp directory", ex);
        }

        try {
            File f = new File(Common.userAccessiblePath);
            if (!f.exists()) f.mkdirs();
        } catch (Exception ex) {
            Log.e(Common.TAG, "Could not create user accessible directory", ex);
        }

        try {
            File f = new File(Common.exportPath);
            if (!f.exists()) f.mkdirs();
        } catch (Exception ex) {
            Log.e(Common.TAG, "Could not create export directory", ex);
        }
    }
}
