CREATE INDEX artist_enabled ON Artist (enabled);
CREATE INDEX artist_enabled_lottery ON Artist (enabled, lottery);
CREATE INDEX artist_enabled_standby ON Artist (enabled, standby);
CREATE INDEX artist_enabled_signedIn ON Artist (enabled, signedIn);
CREATE INDEX artist_enabled_lotteryGuaranteed ON Artist (enabled, lotteryGuaranteed);

CREATE INDEX standByDay_artist_uuid ON StandByDay (artist_uuid);
CREATE index checkedInDay_artist_uuid ON CheckedInDay (artist_uuid);